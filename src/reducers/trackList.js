const initialState = [
    'Smells like Spirit',
    'Enter Sandman'
];

// reducer
export default function trackList(state = initialState, action) {
    switch (action.type) {
        case 'ADD_TRACK':
            return [
                ...state,
                action.payload
            ];
        default:
            return state;
    }
}