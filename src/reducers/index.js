import { combineReducers } from 'redux';

import trackList from './trackList';
import arcticleTitle from './arcticleTitle';
import pizzas from './pizzas'

export default combineReducers({
    trackList,
    arcticleTitle,
    pizzas
})