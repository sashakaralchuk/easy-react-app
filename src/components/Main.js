import React, { Component } from 'react'
import Article from "./Article"
import TrackList from "./TrackList"
import { Route, Switch } from 'react-router-dom'
import Pizzas from './Pizzas'

export default class Main extends Component {
    render() {
        return (
            <Switch>
                <Route path="/article" component={Article} />
                <Route path="/tracklist" component={TrackList} />
                <Route path="/pizzas" component={Pizzas} />
            </Switch>
        )
    }
}