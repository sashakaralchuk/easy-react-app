import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Main extends Component {
    render() {
        return (
            <div>
                <div><Link to="/article">article</Link></div>
                <div><Link to="/tracklist">tracklist</Link></div>
                <div><Link to="/pizzas">pizzas</Link></div>
            </div>
        )
    }
}
