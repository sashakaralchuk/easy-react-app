import React, { Component } from 'react';
import { connect } from 'react-redux';

class Article extends Component {
    onChangeHandler = (e) => {
        this.props.onChangeArticleTitle(e.target.value);
    }

    render() {
        return (
            <div>
                <div>{this.props.arcticleTitle}</div>
                <input
                    value={this.props.arcticleTitle}
                    onChange={this.onChangeHandler}
                    type = "text" />
            </div>
        );
    };
}


export default connect(
    state => ({
        arcticleTitle: state.arcticleTitle
    }),
    dispatch => ({
        onChangeArticleTitle: arcticleTitle => dispatch({
            type: 'CHANGE_ARCTICLE_TITLE',
            payload: arcticleTitle
        })
    })
)(Article);

