import React, { Component } from 'react';
import { connect } from 'react-redux';
import Request from 'superagent';


class Pizzas extends Component {

    componentWillMount() {
        this.props.onGetTracks();
    }

    showState = () => {
        console.log(this.props.pizzas);
    }

    render() {
        return (
            <ul>
                {this.props.pizzas.map((pizza, index) => <li key={index}>{pizza.title}</li>)}
            </ul>
        );
    };
}

export default connect(
    state => ({
        pizzas: state.pizzas
    }),
    dispatch => ({
        onGetTracks: () => {
            // custom http
            Request.get('http://localhost:3003/pizzas')
                .then(response => {
                    dispatch({
                        type: "GET_PIZZAS",
                        payload: response.body
                    })
                });
        }
    })
)(Pizzas);

