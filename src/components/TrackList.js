import React, { Component } from 'react';
import { connect } from 'react-redux';

class TrackList extends Component {
    onAddTrack = () => {
        this.props.onChangeTrackList(this.trackInput.value);
        this.trackInput.value = null;
    }

    render() {
        return (
            <div>
                <input type = "text" ref={input => this.trackInput = input} />
                <button onClick={this.onAddTrack}>Add Track</button>
                <ul>
                    {this.props.trackList.map((track, index) => <li key={index}>{track}</li>)}
                </ul>
            </div>
        );
    };
}


export default connect(
    state => ({
        trackList: state.trackList // it's a state from function articleTitle(state = '', action)
    }),
    dispatch => ({
        onChangeTrackList: track => dispatch({
            type: 'ADD_TRACK',
            payload: track
        })
    })
)(TrackList);

